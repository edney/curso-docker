const http = require('http');
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello, this is the Node.js backend server!\n');
});
server.listen(3000, () => {
  console.log('Node.js server is running on port 3000.');
});