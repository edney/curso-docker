FROM node:14-alpine
WORKDIR /app
COPY package.json package-lock.json ./
#RUN npm config set proxy http://192.168.132.202:8080 ; npm config set proxy https://192.168.132.202:8080 ; npm config set strict-ssl false ; npm install
RUN npm install
COPY  .  .
CMD ["npm", "start"]